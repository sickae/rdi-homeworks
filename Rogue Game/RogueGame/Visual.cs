﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RogueGame.Game.Automate;
using RogueGame.Game.Rules;

namespace RogueGame.Game.Visual
{
    interface IPrintable
    {
        int X { get; }
        int Y { get; }
        char Form { get; }
    }

    interface IDisplayable
    {
        int[] DisplayableSize { get; }

        IPrintable[] DisplayableElements();
    }

    class ConsoleScreen : IAutomatic
    {
        private IDisplayable source;
        private int offx;
        private int offy;
        public int RunInterval { get => 1; }

        public ConsoleScreen(IDisplayable source, int offsetX, int offsetY)
        {
            this.source = source;
            offx = offsetX;
            offy = offsetY;
        }

        public void Display()
        {
            IPrintable[] p = source.DisplayableElements();
            int[] size = source.DisplayableSize;
            for (int i = 0; i < size[0]; i++)
            {
                for (int j = 0; j < size[1]; j++)
                {
                    IPrintable e = null;
                    for (int k = 0; k < p.Length; k++)
                    {
                        if (p[k].X == i && p[k].Y == j) e = p[k];
                    }
                    if (e != null) ThreadSafeConsole.PrintXY(e.X + offx, e.Y + offy, e.Form);
                    else ThreadSafeConsole.PrintXY(i, j,' ');
                }
            }
        }

        public void Run() => Display();
    }

    class ConsoleScoreboard
    {
        private int offx;
        private int offy;
        private int maxLines;
        private int line;

        public ConsoleScoreboard(int offsetX, int offsetY, int maxLines)
        {
            offx = offsetX;
            offy = offsetY;
            this.maxLines = maxLines;
        }

        void PlayerChanged(Player sender, int score, int health)
        {
            ThreadSafeConsole.PrintXY(sender.X + offx, sender.Y + offy, $"Player name: {sender.Name}, Health: {health}, Score: {score}");
            if (line++ == maxLines) line = 0;
        }

        public void AddPlayer(Player player) => player.Handler += PlayerChanged;
    }
}
