﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RogueGame.Game.Space;
using RogueGame.Game.Visual;
using RogueGame.Game.Automate;

namespace RogueGame.Game.Rules
{
    delegate void TreasurePickup(Treasure treasure, Player player);
    delegate void PlayerChange(Player sender, int score, int health);

    class Wall : FixedGameElement, IPrintable
    {
        public override double Size { get => 1; }
        public char Form { get => '\u2593'; }

        public Wall(int x, int y, GameSpace space) : base(x, y, space) { }

        public override void Collide(GameElement e) { }
    }

    class Player : MovingGameElement, IPrintable, IDisplayable
    {
        private string name;
        private int health;
        private int score;
        public string Name { get { return name; } }
        public override double Size { get => 0.2; }
        public virtual char Form { get { return Active ? '\u263A' : '\u263B'; } }
        public int[] DisplayableSize { get => new int[] { space.SizeX, space.SizeY }; }
        public event PlayerChange Handler;

        public Player(string name, int x, int y, GameSpace space) : base(x, y, space)
        {
            this.name = name;
            health = 100;
            score = 0;
        }

        public override void Collide(GameElement e) { }

        public void Injure(int amount)
        {
            if (health > 0) health -= health - amount > 0 ? amount : health;
            if (health == 0) Active = false;
            Handler?.Invoke(this, score, health);
        }

        public void GetScore(int amount)
        {
            score += amount;
            Handler?.Invoke(this, score, health);
        }

        public void Move(int dx, int dy) => Replace(X + dx, Y + dy);

        public IPrintable[] DisplayableElements()
        {
            GameElement[] el = space.GetElements(X, Y, 5);
            List<IPrintable> pl = new List<IPrintable>();
            for (int i = 0; i < el.Length; i++)
                if (el[i] is IPrintable) pl.Add(el[i] as IPrintable);
            return pl.ToArray();
        }
    }

    class Treasure : FixedGameElement, IPrintable
    {
        private int value;
        public override double Size { get => 1; }
        public char Form { get => '\u2666'; }
        public event TreasurePickup Handler;

        public Treasure(int x, int y, GameSpace space) : base(x, y, space) { value = 50; }

        public override void Collide(GameElement e)
        {
            if(e is Player)
            {
                (e as Player).GetScore(value);
                space.RemoveElement(this);
                Handler?.Invoke(this, e as Player);
            }
        }
    }
    
    class AIPlayer : Player, IAutomatic
    {
        static Random rnd = new Random();
        public override char Form { get => '\u2640'; }
        public int RunInterval { get => 2; }

        public AIPlayer(string name, int x, int y, GameSpace space) : base(name, x, y, space) { }

        public void RandomMove()
        {
            int r = rnd.Next(0, 4);
            if (r == 0) Move(-1, 0);
            if (r == 1) Move(1, 0);
            if (r == 2) Move(0, -1);
            if (r == 3) Move(0, 1);
        }

        public void Run() => RandomMove();
    }

    class EvilAIPlayer : AIPlayer
    {
        public override char Form { get => '\u2642'; }

        public EvilAIPlayer(string name, int x, int y, GameSpace space) : base(name, x, y, space) { }

        public override void Collide(GameElement e)
        {
            //base.Collide(e);
            if (Active && e is Player)
            {
                (e as Player).Injure(10);
                Injure(10);
            }
        }
    }
}
