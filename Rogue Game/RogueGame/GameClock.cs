﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RogueGame.Game.Automate;

namespace RogueGame.Game.Automate
{
    class GameClock
    {
        List<IAutomatic> methods = new List<IAutomatic>();
        Timer timer;

        public GameClock()
        {
            timer = new Timer(Activate, null, 0, 100);
        }

        int counter = 0;
        bool inProgress = false;
        public void Activate(Object state)
        {
            // ha tul lassu lenne a kirajzolas, es az elozo orajel feldolgozasa elott ujra meghivodna, akkor azt atlepi
            // nem lock, mivel ilyenkor szandekosan kihagyja ezt a ciklust, igy nem fog feltorlodni
            if (!inProgress)
            {
                inProgress = true;
                // szamoljuk, hogy hanyadik orajelnel jarunk
                counter++;
                // akinek most epp aktualis, az kap egy orajelet
                for (int i = 0; i < methods.Count; i++)
                    if (counter % methods[i].RunInterval == 0)
                        methods[i].Run();

                inProgress = false;
            }
        }

        // feliratkozas
        public void Add(IAutomatic method) => methods.Add(method);

        // feliratkozas
        public void Remove(IAutomatic method) => methods.Remove(method);
    }
}
