﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Xml.Linq;

namespace GoogleMapsGeocoding
{
    class Program
    {
        const string API_KEY = "AIzaSyDzxglWIu_WdZ6Sop0lTvvRjO7kOLTbwc4";
        static SemaphoreSlim sem = new SemaphoreSlim(4);
        static int all, current;
        static string[] cont;

        static void Main(string[] args)
        {
            cont = File.OpenText(@"D:\RDI\Files\famousplaces.txt").ReadToEnd().Split('\n');
            List<Task<PointOfInterest>> ts = new List<Task<PointOfInterest>>();
            all = cont.Length;
            foreach (string e in cont)
            {
                Task<PointOfInterest> t = new Task<PointOfInterest>(() => GetPlace(e));
                ts.Add(t);
                t.Start();
            }
            Task c = new Task(() => UpdateConsole(), TaskCreationOptions.LongRunning);
            c.Start();

            Task f = null;
            Task.WhenAll(ts.ToArray()).ContinueWith(x =>
            {
                c.Wait();
                f = new Task(() =>
                {
                    PointOfInterest poi = new PointOfInterest();
                    do
                    {
                        Console.Clear();
                        if (poi.Latitude == -1 && poi.Longitude == -1)
                            Console.WriteLine("Invalid location!");
                        Console.Write("Where are you now? - ");
                        poi = GetPlace(Console.ReadLine());
                    }
                    while (poi.Latitude == -1 && poi.Longitude == -1);
                    Console.WriteLine("\nTop 10 closest points of interest: ");
                    List<PointOfInterest> q = x.Result.OrderBy(y => Haversine(y.Latitude, y.Longitude, poi.Latitude, poi.Longitude)).Take(10).ToList();
                    foreach (var e in q)
                        Console.WriteLine(e.Name);
                });
                f.Start();
            });
            Console.ReadKey();
            Console.ReadKey();
        }

        static string FormatUrl(string place) =>
            string.Format($"https://maps.googleapis.com/maps/api/geocode/xml?address={place}&key={API_KEY}");

        static PointOfInterest GetPlace(string place)
        {
            sem.Wait();
            string url = FormatUrl(place);
            XDocument xd = XDocument.Load(url);
            if (xd.Descendants("status").Single().Value == "ZERO_RESULTS")
                return new PointOfInterest() { Latitude = -1, Longitude = -1 };
            while (xd.Descendants("status").Single().Value != "OK")
            {
                Thread.Sleep(100);
                xd = XDocument.Load(url);
            }
            sem.Release();
            current++;
            return new PointOfInterest()
            {
                Name = place,
                Latitude = double.Parse(xd.Descendants("location").First().Element("lat").Value),
                Longitude = double.Parse(xd.Descendants("location").First().Element("lng").Value)
            };
        }

        struct PointOfInterest
        {
            public string Name { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
        }

        static double Haversine(double lat1, double lng1, double lat2, double lng2)
        {
            const double r = 6371e3; 
            var dlat = (lat2 - lat1) / 2;
            var dlon = (lng2 - lng1) / 2;
            var q = Math.Pow(Math.Sin(dlat), 2) + Math.Cos(lat1) * Math.Cos(lat2) * Math.Pow(Math.Sin(dlon), 2);
            var c = 2 * Math.Atan2(Math.Sqrt(q), Math.Sqrt(1 - q));
            var d = r * c;
            return d / 1000;
        }

        static void UpdateConsole()
        {
            Console.CursorVisible = false;
            while (current <= cont.Length)
            {
                if (current == cont.Length)
                    break;
                int x = current * 100 / cont.Length;
                Console.SetCursorPosition(0, 0);
                Console.WriteLine($"Downloading data... {x}%");
                Thread.Sleep(50);
            }
            Console.CursorVisible = true;
        }
    }
}
