﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Mondial
{
    class Program
    {
        static void Main(string[] args)
        {
            XDocument xd = XDocument.Load("http://www.szabozs.hu/_DEBRECEN/kerteszg/mondial-3.0.xml");

            var continents = xd.Descendants("continent")?.Select(x => new Continent()
            {
                ID = x.Attribute("id")?.Value,
                Name = x.Attribute("name")?.Value
            });

            var countries = xd.Descendants("country")?.Select(x => new Country()
            {
                ID = x.Attribute("id")?.Value,
                Name = x.Attribute("name")?.Value,
                CapitalID = x.Attribute("name")?.Value,
                Government = x.Attribute("government")?.Value,
                Population = int.Parse(x.Attribute("population")?.Value),
                Cities = x.Descendants("city")?.Select(y => new City()
                {
                    ID = y.Attribute("id")?.Value,
                    CountryID = y.Attribute("country")?.Value,
                    Name = y.Attribute("name")?.Value,
                    /*LocatedAt = y.Descendants("located_at")?.Select(z => new          // Key already defined exception
                    {
                        Water = z.Attribute("water")?.Value,
                        Type = z.Attribute("type")?.Value
                    }).ToDictionary(d => d.Water, d => d.Type)*/
                    LocatedAt = y.Descendants("located_at")?.Select(z => new Water()
                    {
                        ID = z.Attribute("water")?.Value,
                        Type = z.Attribute("type")?.Value
                    }).ToList()
                }).ToList(),
                EthnicGroups = x.Descendants("ethnicgroups")?.Select(y => new Ehtnicity()
                {
                    Name = y?.Value,
                    Percentage = double.Parse(y.Attribute("percentage")?.Value.Replace('.',',')),
                    Population = (int)(double.Parse(y.Attribute("percentage")?.Value.Replace('.',',')) / 100 * int.Parse(x.Attribute("population")?.Value))
                }).ToList(),
                Religions = x.Descendants("religions")?.Select(y => new
                {
                    Religion = y?.Value,
                    Percentage = double.Parse(y.Attribute("percentage")?.Value.Replace('.',','))
                }).ToDictionary(d => d.Religion, d => d.Percentage),
                Borders = x.Descendants("border")?.Select(y => new
                {
                    Length = double.Parse(y.Attribute("length")?.Value.Replace('.',',')),
                    Country = y.Attribute("country")?.Value
                }).ToDictionary(d => d.Country, d => d.Length)
            });

            var cities = xd.Descendants("city")?.Select(x => new City()
            {
                ID = x.Attribute("id")?.Value,
                CountryID = x.Attribute("country")?.Value,
                Name = x.Attribute("name")?.Value,
            });

            var rivers = xd.Descendants("river")?.Select(x => new Water()
            {
                ID = x.Attribute("id")?.Value,
                Name = x.Attribute("name")?.Value,
                CountryIDs = x.Descendants("located")?.Select(y => y.Attribute("country")?.Value).ToList(),
                To = x.Descendants("to")?.Select(y => new Water()
                {
                    ID = y.Attribute("water")?.Value,
                    Type = y.Attribute("type")?.Value
                }).ToList()
            });

            var seas = xd.Descendants("sea")?.Select(x => new Water()
            {
                ID = x.Attribute("id")?.Value,
                Name = x.Attribute("name")?.Value,
                CountryIDs = x.Descendants("located")?.Select(y => y.Attribute("country")?.Value).ToList(),
                From = xd.Descendants("to")?.Where(y => y.Attribute("water")?.Value == x.Attribute("id")?.Value).Select(y => new Water()
                {
                    ID = y.Parent.Attribute("id")?.Value
                }).ToList()
            });

            Console.WriteLine("[(1) Melyik ország népességsűrűsége a legnagyobb?  ]");

            // 1. Melyik ország népességsűrűsége a legnagyobb?
            var q1 = countries.OrderByDescending(x => x.Population).First();
            Console.WriteLine(q1.Name + ": " + q1.Population);

            Console.WriteLine("\n[(2) Az országok hány százalékában van valamilyen formájú demokrácia?  ]");

            // 2. Az országok hány százalékában van valamilyen formájú demokrácia?
            var q2 = countries.Where(x => x.Government.Contains("democracy")).Count() / (double)countries.Count() * 100;
            Console.WriteLine(q2 + "%");

            Console.WriteLine("\n[(3) Vallási és etnikai csoportok száma országonként, összegük szerinti csökkenő sorrendben  ]");

            // 3. Vallási és etnikai csoportok száma országonként, összegük szerinti csökkenő sorrendben
            var q3 = countries.Select(x => new
            {
                Country = x.Name,
                Ethnicity = x.EthnicGroups,
                Religion = x.Religions
            }).OrderByDescending(x => x.Ethnicity.Count + x.Religion.Count);
            foreach (var e in q3)
                Console.WriteLine(e.Country +
                    "\n\tEthnic groups: " + e.Ethnicity.Count +
                    "\n\tReligions: " + e.Religion.Count);

            Console.WriteLine("\n[(4) Határok hossza országonként  ]");

            // 4. Határok hossza országonként
            var q4 = countries.Select(x => new
            {
                Country = x.Name,
                Borders = x.Borders
            });
            foreach (var e in q4)
            {
                Console.WriteLine(e.Country);
                foreach (var i in e.Borders)
                    Console.WriteLine("\tLength of border with " + countries.FirstOrDefault(x => x.ID == i.Key)?.Name + ": " + i.Value);
            }

            Console.WriteLine("\n[(5) A legtöbb országot metsző 5 folyó  ]");

            // 5. A legtöbb országot metsző 5 folyó
            var q5 = rivers.OrderByDescending(x => x.CountryIDs.Count).Take(5);
            foreach (var e in q5)
                Console.WriteLine(e.Name);

            Console.WriteLine("\n[(6) Azon 5 ország ahol a legnagyobb számban találunk folyót, tavat és tengert összesen  ]");

            // 6. Azon 5 ország ahol a legnagyobb számban találunk folyót, tavat és tengert összesen
            var q6 = countries.OrderByDescending(x => x.Cities.Select(y => y.LocatedAt).Count()).Take(5);
            foreach (var e in q6)
                Console.WriteLine(e.Name);

            Console.WriteLine("\n[(7) Mely tengerekbe folyik a legtöbb folyó?  ]");

            // 7. Mely tengerekbe folyik a legtöbb folyó?
            var q7 = seas.OrderByDescending(x => x.From.Where(y => rivers.Any(z => z.ID == y.ID) && x.Name.Contains("Sea")).Count()).Take(5);
            foreach (var e in q7)
                Console.WriteLine(e.Name);

            Console.WriteLine("\n[(8) A világ etnikumainak mérete (ország lakossága * etnikai csoport aránya)  ]");

            // 8. A világ etnikumainak mérete (ország lakossága * etnikai csoport aránya)
            var q8 = countries.SelectMany(x => x.EthnicGroups).GroupBy(x => x.Name).Select(x => new
            {
                Name = x.Key,
                Population = x.Sum(y => y.Population)
            }).OrderBy(x => x.Name);
            foreach (var e in q8)
                Console.WriteLine(e.Name + ": " + e.Population);

            Console.ReadKey();
        }
    }

    class Continent
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }

    class Country
    {
        public string ID { get; set; }
        public string CapitalID { get; set; }
        public string Name { get; set; }
        public string Government { get; set; }
        public int Population { get; set; }
        public List<City> Cities { get; set; }
        public List<Ehtnicity> EthnicGroups { get; set; }
        //public Dictionary<string, double> EthnicGroups { get; set; } // Key = ethnicity, Value = percentage
        public Dictionary<string, double> Religions { get; set; } // Key = religion, Value = percentage
        public Dictionary<string, double> Borders { get; set; } // Key = country id, Value = length
    }

    class City
    {
        public string ID { get; set; }
        public string CountryID { get; set; }
        public string Name { get; set; }
        //public Dictionary<string, string> LocatedAt { get; set; } // Key = water id, Value = type
        public List<Water> LocatedAt { get; set; }
    }

    class Water
    {
        public string ID { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public List<string> CountryIDs { get; set; }
        public List<Water> To { get; set; }
        public List<Water> From { get; set; }
    }

    class Ehtnicity
    {
        public string Name { get; set; }
        public double Percentage { get; set; }
        public int Population { get; set; }
    }
}
