﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HA.Client.SignalR
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://localhost:8080";
            var hubConnection = new HubConnection(url);
            var hubProxy = hubConnection.CreateHubProxy("StudentHub");

            Console.WriteLine("Press ENTER...");
            Console.ReadKey();

            hubProxy.On("Refresh", () =>
            {
                Console.WriteLine("REFRESH CALL FROM SERVER");
                RefreshList(hubProxy);
            });

            hubProxy.On("NewStudent", param => {
                Console.WriteLine("NEW STUDENT FROM SERVER: " + param);
            });

            hubConnection.Start().ContinueWith(task =>
            {
                if (task.IsFaulted)
                    Console.WriteLine("ERROR: " + task.Exception.GetBaseException());
                else
                    Console.WriteLine("CONNECTED");
            }).Wait();

            Console.ReadKey();
            RefreshList(hubProxy);
            Console.ReadKey();

            Console.WriteLine("\nADDING...\n");
            Student s = new Student()
            {
                Id = 60,
                Fname = "Test",
                Lname = "SignalR"
            };
            hubProxy.Invoke("AddStudent", s).Wait();
            Console.WriteLine("ADDED");
            RefreshList(hubProxy);
            Console.ReadKey();

            Console.WriteLine("\nDELETING...\n");
            hubProxy.Invoke("RemoveStudent", 12).Wait();
            Console.WriteLine("DELETED");
            Console.ReadKey();

            hubConnection.Stop();
            Console.WriteLine("STOPPED");
            Console.ReadKey();
        }

        static async void RefreshList(IHubProxy signalr)
        {
            Console.WriteLine("FETCHING FROM SIGNALR");
            var list = await signalr.Invoke<IEnumerable<Student>>("GetStudents");
            foreach (var i in list)
                Console.WriteLine(i);
        }
    }
}
