﻿using HA.Client.Soap.HASoap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HA.Client.Soap
{
    class Program
    {
        static void Main(string[] args)
        {
            StudentServiceClient client = new StudentServiceClient();

            Console.WriteLine("Press ENTER...");
            Console.ReadKey();

            foreach(StudentDTO dto in client.GetStudents())
                Console.WriteLine(dto.Id + ": " + dto.Lname + " " + dto.Fname);

            Console.WriteLine("\nPress ENTER...\n");
            Console.ReadKey();

            StudentDTO s = new StudentDTO()
            {
                Id = 60,
                Fname = "Test",
                Lname = "Soap"
            };
            client.AddStudent(s);
            foreach (StudentDTO dto in client.GetStudents())
                Console.WriteLine(dto.Id + ": " + dto.Lname + " " + dto.Fname);

            Console.WriteLine("\nPress ENTER...\n");
            Console.ReadKey();

            client.RemoveStudent(12);
            foreach (StudentDTO dto in client.GetStudents())
                Console.WriteLine(dto.Id + ": " + dto.Lname + " " + dto.Fname);

            Console.WriteLine("\nEXITING...");
            Console.ReadKey();

        }
    }
}
