﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HA.Service.SignalR.ConsoleApp
{
    public class StudentDTO
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Fname { get; set; }
        [DataMember]
        public string Lname { get; set; }
    }
}
