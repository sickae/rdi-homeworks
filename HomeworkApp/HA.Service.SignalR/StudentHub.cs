﻿using AutoMapper;
using HA.Logic;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HA.Service.SignalR.ConsoleApp
{
    public class StudentHub : Hub
    {
        ILogic logic;
        IMapper mapper;

        public StudentHub()
        {
            logic = new RealLogic();
            mapper = MapperFactory.CreateMapper();
        }

        public void RemoveStudent(int id)
        {
            logic.RemoveStudent(id);
            Clients.All.Refresh();
        }

        public void AddStudent(StudentDTO student)
        {
            Student s = mapper.Map<StudentDTO, Student>(student);
            logic.AddStudent(s);
            Clients.All.NewStudent(student);
        }

        public IEnumerable<StudentDTO> GetStudents()
        {
            List<Student> l = logic.GetStudents();
            return mapper.Map<List<Student>, IEnumerable<StudentDTO>>(l);
        }
    }
}
