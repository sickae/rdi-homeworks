﻿using HA.Repo.Exceptions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HA.Repo.Generic
{
    public abstract class EFRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected DbContext context;

        public EFRepository(DbContext ctx)
        {
            context = ctx;
            context.Database.Log = Console.WriteLine;
        }

        public abstract TEntity GetById(int id);

        public void Delete(int id)
        {
            TEntity e = GetById(id);
            if (e == null) throw new NoDataFoundException("No data found!");
            Delete(e);
        }

        public void Delete(TEntity entity)
        {
            context.Set<TEntity>().Remove(entity);
            context.SaveChanges();
        }

        public void Dispose() => context.Dispose();

        public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> condition) => context.Set<TEntity>().Where(condition);

        public IQueryable<TEntity> GetAll() => context.Set<TEntity>();

        public void Insert(TEntity entity)
        {
            context.Set<TEntity>().Add(entity);
            context.SaveChanges();
        }
    }
}
