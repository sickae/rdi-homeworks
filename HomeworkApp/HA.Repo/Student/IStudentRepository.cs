﻿using HA.Data;
using HA.Repo.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HA.Repo.Student
{
    public interface IStudentRepository : IRepository<STUDENTS>
    {
        void Modify(int id, string new_fname, string new_lname);
    }
}
