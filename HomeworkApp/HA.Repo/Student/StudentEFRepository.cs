﻿using HA.Data;
using HA.Repo.Exceptions;
using HA.Repo.Generic;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HA.Repo.Student
{
    public class StudentEFRepository : EFRepository<STUDENTS>, IStudentRepository
    {
        public StudentEFRepository(DbContext ctx) : base(ctx) { }

        public override STUDENTS GetById(int id) => Get(x => x.ID == id).SingleOrDefault();

        public void Modify(int id, string new_fname, string new_lname)
        {
            STUDENTS s = GetById(id);
            if (s == null) throw new NoDataFoundException("No data found!");
            if (new_fname != null) s.FNAME = new_fname;
            if (new_lname != null) s.LNAME = new_lname;
            context.SaveChanges();
        }
    }
}
