﻿using HA.Repo.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HA.Repo
{
    public class MyRepository
    {
        public IStudentRepository StudentRepo { get; private set; }

        public MyRepository(IStudentRepository studentRepo) => StudentRepo = studentRepo;
    }
}
