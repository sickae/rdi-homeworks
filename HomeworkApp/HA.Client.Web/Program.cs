﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HA.Client.Web
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://localhost:64797/api/student/";
            Console.WriteLine("WAITING...");
            Console.ReadKey();

            using (WebClient client = new WebClient())
            {
                // ALL
                string json = client.DownloadString(url + "all");
                var list = JsonConvert.DeserializeObject<List<Student>>(json);
                foreach(var i in list)
                    Console.WriteLine(i.ToString());

                Console.ReadKey();
                NameValueCollection postData;
                byte[] responseBytes;

                // ADD
                postData = new NameValueCollection();
                postData.Add("id", "60");
                postData.Add("fname", "Test");
                postData.Add("lname", "WebClient");
                responseBytes = client.UploadValues(url + "add", "POST", postData);
                Console.WriteLine("ADD: " + Encoding.UTF8.GetString(responseBytes));
                Console.WriteLine("ALL : " + client.DownloadString(url + "all"));

                Console.ReadKey();

                // MODIFY
                int n = list.Max(x => x.Id) + 1;
                postData = new NameValueCollection();
                postData.Add("id", n.ToString());
                postData.Add("fname", "Modified_Test");
                postData.Add("lname", "WebClient");
                responseBytes = client.UploadValues(url + "modify", "POST", postData);
                Console.WriteLine("MODIFY: " + Encoding.UTF8.GetString(responseBytes));
                Console.WriteLine("ALL: " + client.DownloadString(url + "all"));

                Console.ReadKey();

                // REMOVE
                Console.WriteLine("REMOVE: " + client.DownloadString(url + "remove/" + n));
                Console.WriteLine("ALL: " + client.DownloadString(url + "all"));

                Console.WriteLine("\nEXITING...");
            }
            Console.ReadKey();
        }
    }
}
