﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HA.Client.Web
{
    class Student
    {
        public int Id { get; set; }
        public string Fname { get; set; }
        public string Lname { get; set; }

        public override string ToString() => $"#{Id} {Lname} {Fname}";
    }
}
