﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace HA.Service.Soap
{
    [ServiceContract]
    public interface IStudentService
    {
        [OperationContract]
        List<StudentDTO> GetStudents();

        [OperationContract]
        bool AddStudent(StudentDTO student);

        [OperationContract]
        bool RemoveStudent(int id);
    }
}
