﻿using AutoMapper;
using HA.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace HA.Service.Soap
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Single,
        InstanceContextMode = InstanceContextMode.PerSession)]
    public class StudentService : IStudentService
    {
        ILogic logic;
        IMapper mapper;

        public StudentService()
        {
            logic = new RealLogic();
            mapper = MapperFactory.CreateMapper();
        }

        public bool AddStudent(StudentDTO student)
        {
            try
            {
                Student s = mapper.Map<StudentDTO, Student>(student);
                logic.AddStudent(s);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<StudentDTO> GetStudents()
        {
            List<Student> l = logic.GetStudents();
            return mapper.Map<List<Student>, List<StudentDTO>>(logic.GetStudents());
        }

        public bool RemoveStudent(int id)
        {
            try
            {
                logic.RemoveStudent(id);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
