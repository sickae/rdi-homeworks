﻿using AutoMapper;
using HA.Data;
using HA.Repo;
using HA.Repo.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HA.Logic
{
    public class RealLogic : ILogic
    {
        protected MyRepository repo;
        IMapper mapper;

        public RealLogic()
        {
            StudentsEntities se = new StudentsEntities();
            var studentRepo = new StudentEFRepository(se);
            repo = new MyRepository(studentRepo);
            mapper = MapperFactory.CreateMapper();
        }

        public void AddStudent(Student student)
        {
            STUDENTS s = mapper.Map<Student, STUDENTS>(student);
            repo.StudentRepo.Insert(s);
        }

        public Student GetStudent(int id)
        {
            STUDENTS s = repo.StudentRepo.GetById(id);
            return mapper.Map<STUDENTS, Student>(s);
        }

        public List<Student> GetStudents()
        {
            IQueryable<STUDENTS> l = repo.StudentRepo.GetAll();
            return mapper.Map<IQueryable<STUDENTS>, List<Student>>(l);
        }

        public void ModifyStudent(int id, string new_fname, string new_lname) => repo.StudentRepo.Modify(id, new_fname, new_lname);

        public void RemoveStudent(int id) => repo.StudentRepo.Delete(id);
    }
}
