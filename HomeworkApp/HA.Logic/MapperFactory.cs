﻿using AutoMapper;
using HA.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HA.Logic
{
    public class MapperFactory
    {
        public static IMapper CreateMapper() => new MapperConfiguration(x =>
            x.CreateMap<STUDENTS, Student>().ReverseMap()).CreateMapper();
    }
}
