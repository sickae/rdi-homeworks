﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HA.Logic
{
    public interface ILogic
    {
        void AddStudent(Student student);
        void RemoveStudent(int id);
        void ModifyStudent(int id, string new_fname, string new_lname);

        List<Student> GetStudents();
        Student GetStudent(int id);
    }
}
