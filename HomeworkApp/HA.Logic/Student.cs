﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HA.Logic
{
    public class Student
    {
        public int ID { get; set; }
        public string FNAME { get; set; }
        public string LNAME { get; set; }
    }
}
