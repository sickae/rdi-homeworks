﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HA.Web.Views.Student
{
    public class StudentDTO
    {
        public int Id { get; set; }
        public string Fname { get; set; }
        public string Lname { get; set; }
    }
}