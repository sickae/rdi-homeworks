﻿using AutoMapper;
using HA.Logic;
using HA.Web.Views.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace HA.Web.Controllers
{
    public class StudentController : ApiController
    {
        ILogic logic;
        IMapper mapper;

        public StudentController()
        {
            logic = new RealLogic();
            mapper = MapperFactory.CreateMapper();
        }

        [HttpGet] // GET /api/student/all
        [ActionName("all")]
        public IEnumerable<StudentDTO> GetAllStudents()
        {
            List<Student> l = logic.GetStudents();
            return mapper.Map<List<Student>, IEnumerable<StudentDTO>>(l);
        }

        [HttpGet] // GET /api/student/remove/42
        [ActionName("remove")]
        public int RemoveStudent(int id)
        {
            logic.RemoveStudent(id);
            return 1;
        }

        [HttpPost] // POST api/student/add
        [ActionName("add")]
        public int AddStudent(StudentDTO student)
        {
            Student s = mapper.Map<StudentDTO, Student>(student);
            logic.AddStudent(s);
            return student.Id;
        }

        [HttpPost] // POST api/student/modify
        [ActionName("modify")]
        public int ModifyStudent(StudentDTO student)
        {
            logic.ModifyStudent(student.Id, student.Fname, student.Lname);
            return student.Id;
        }
    }
}