﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HA.Utils
{
    public class MyProvider : IDisposable
    {
        DbConnection server;

        public MyProvider(string connection_string)
        {
            server = new SqlConnection(connection_string);
            server.Open();
        }

        public void Dispose() => server.Close();

        DbCommand InitCommand(string sql, Dictionary<string, string> parameters)
        {
            DbCommand cmd = new SqlCommand(sql, server as SqlConnection);

            if (parameters != null)
            {
                foreach(var i in parameters)
                {
                    DbParameter p = cmd.CreateParameter();
                    p.ParameterName = i.Key;
                    p.Value = i.Value;
                    cmd.Parameters.Add(p);
                }
            }

            return cmd;
        }

        public int ExecuteNonQuery(string sql, Dictionary<string, string> parameters = null)
        {
            DbCommand cmd = InitCommand(sql, parameters);
            return cmd.ExecuteNonQuery();
        }

        public object ExecuteScalar(string sql, Dictionary<string, string> parameters = null)
        {
            DbCommand cmd = InitCommand(sql, parameters);
            return cmd.ExecuteScalar();
        }

        public MyResult ExecuteSelect(string sql, Dictionary<string, string> parameters = null)
        {
            DbCommand cmd = InitCommand(sql, parameters);
            DbDataReader reader = cmd.ExecuteReader();
            MyResult output = new MyResult();
            string[] fields = null;
            object[] values = null;

            while (reader.Read())
            {
                if (fields == null)
                {
                    fields = new string[reader.FieldCount];
                    values = new string[reader.FieldCount];
                    for (int i = 0; i < reader.FieldCount; i++)
                        fields[i] = reader.GetName(i).ToUpper();
                }

                reader.GetValues(values);
                output.AddRow(fields, values);
            }

            if (reader != null && !reader.IsClosed)
                reader.Close();

            return output;
        }
    }
}
