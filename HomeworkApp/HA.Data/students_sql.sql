﻿create table STUDENTS(
	ID int identity,
	FNAME varchar(50) not null,
	LNAME varchar(50) not null,
	primary key(ID)
);

insert into STUDENTS values('Attila', 'Némethy');
insert into STUDENTS values('Bence', 'Jurás');
insert into STUDENTS values('Gergő', 'Hajzer');
insert into STUDENTS values('Márk', 'Molnár');
insert into STUDENTS values('Tamás', 'Nagy');
insert into STUDENTS values('Kristóf', 'Pászti-Nagy');
insert into STUDENTS values('Ferenc', 'Sándor');
insert into STUDENTS values('Szabolcs', 'Sályi');
insert into STUDENTS values('Dávid', 'Végh');
insert into STUDENTS values('Álmos', 'Bánhegyi');
insert into STUDENTS values('Bence', 'Sipos');